#!/bin/sh
# Copyright (C) 2011, Julia Longtin
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#

# apache status plugin, for SNMPd.

#time to cache contents, in seconds.
CACHETIME=30

CACHEDIR=/var/cache/apache2/
CACHEFILE=apache-status.cache

if [ ! -d "$CACHEDIR" ]; then
    {
	mkdir -p $cachedir
	result=$?
	if [ $result != 0 ]; then
	    {
		echo "unable to create cache directory $CACHEDIR!"
		return 1
	    }
	fi
    }
fi

if [ ! -f "$CACHEDIR/$CACHEFILE" ]; then
    {
	CACHELASTMOD=0;
	touch $CACHEDIR/$CACHEFILE;
	result=$?
	if [ $result != 0 ]; then
	    {
		echo "unable to create cache file $CACHEDIR/$CACHEFILE!"
		return 2
	    }
	fi
    }
else
    {
	CACHELASTMOD=`stat -c %X $CACHEDIR/$CACHEFILE`
    }
fi

# ok, there is a cache directory, and a file in it.
# check the last modified time against the current time.
if [ "$CACHELASTMOD" -lt "$((`date +%s`-$CACHETIME))" ]; then
    {
	wget "http://127.0.0.1/server-status?mode=auto" -O "$CACHEDIR/$CACHEFILE" -q
	result=$?
	if [ $result != 0 ]; then
	    {
		echo "wget returned $result trying to update $CACHEDIR/$CACHEFILE!"
		return 3
	    }
	fi
    }
fi

fields="Total Accesses|Total kBytes|CPULoad|Uptime|ReqPerSec|BytesPerSec|BytesPerReq|BusyWorkers|IdleWorkers"
keys="_ S R W K D C L G I ."
#separate input into a series of lines.
IFS="
"
for each in `cat $CACHEDIR/$CACHEFILE`
do
    {
	fieldname=`echo $each | sed "s/\(.*\): .*/\1/"`
	fieldcontents=`echo $each | sed "s/.*: \(.*\)/\1/"`
	if [ "$fieldname" = "Scoreboard" ]; then
	    {
		IFS=" "
		i=0;
		for key in $keys; do
		    {
			i=$(($i+1))
			eval part$i=`echo \$fieldcontents| grep -o \$key | wc -l`
		    }
		done;
		IFS="
"
	    }
	else
	    {
		i=0;
		IFS="|"
		for field in $fields; do
		    {
			i=$(($i+1))
			if [ "$field" = "$fieldname" ]; then
			    eval data$i="$fieldcontents"
			fi
		    }
		done;
		IFS="
"
	    }
	fi
    }
done;

i=0;
IFS="|"
for field in $fields; do
    {
	i=$(($i+1))
	s=`eval echo \\$data$i`
	if [ -z "$s" ]; then
	    echo U
	else
	    {
		if [ "$field" = "Total kBytes" ]; then
		    s=$(($s*1024))
		fi
		echo $s
	    }
	fi
    }
done;
IFS=" "
i=0;
for key in $keys; do
    {
	i=$(($i+1))
	eval echo \$part$i
    }
done;