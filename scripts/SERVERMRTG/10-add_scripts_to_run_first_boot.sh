#!/bin/sh

# copy in a script that should set up mrtg.
fcopy /etc/init.d/setup_mrtg

# register it with the init system.
$ROOTCMD insserv -d setup_mrtg