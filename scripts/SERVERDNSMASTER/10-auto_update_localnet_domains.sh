#!/bin/sh

# scripts for performing the dynamic update
fcopy -m root,root,0755 /root/scripts/SERVERDNS/dynamic-dns-update.sh /root/scripts/SERVERDNS/dynamic-dns-update-reverse.sh /root/scripts/SERVERDNS/update_localnet_apache_zones.sh 

ln -s /root/scripts/SERVERDNS/update_localnet_apache_zones.sh $target/etc/dhcp3/dhclient-exit-hooks.d/update_dns

# fcopy /etc/pump.conf 

