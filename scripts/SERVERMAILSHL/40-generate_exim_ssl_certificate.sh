#! /bin/sh

# let people on the local network relay mail through this server

## Our "die" function (think perl)
function die () { echo "$@" 1>&2 ; exit 1 ; }

# check to make sure our variables have been declared
[ ! -n "$SSL_LOCATION_COUNTRY" ] && { die "ERROR: SSL_LOCATION_COUNTRY NOT FOUND!" ; }
[ ! -n "$SSL_LOCATION_STATE" ] && { die "ERROR: SSL_LOCATION_STATE NOT FOUND!" ; }
[ ! -n "$SSL_LOCATION_LOCALITY" ] && { die "ERROR: SSL_LOCATION_LOCALITY NOT FOUND!" ; }
[ ! -n "$SSL_ORG_NAME" ] && { die "ERROR: SSL_ORG_NAME NOT FOUND!" ; }
[ ! -n "$SSL_OU_NAME" ] && { die "ERROR: SSL_OU_NAME NOT FOUND!" ; }
[ ! -n "$SSL_SERVER_NAME" ] && { die "ERROR: SSL_SERVER_NAME NOT FOUND!" ; }
[ ! -n "$SSL_CONTACT_EMAIL" ] && { die "ERROR: SSL_CONTACT_EMAIL NOT FOUND!" ; }

DIR=/etc/exim4
CERT=$DIR/exim.crt
KEY=$DIR/exim.key

DAYS=1095
SSLEAY="$(tempfile -d /target/tmp/ -m600 -pexi)"
# we can't write in /root/.rnd?
RANDFILE="$(tempfile -d /target/tmp/ -m600 -pexi)"
TMPRANDFILE=`echo $RANDFILE | sed "s=/target=="`
cat > $SSLEAY <<EOF
RANDFILE = $TMPRANDFILE
[ req ]
default_bits = 1024
default_keyfile = exim.key
distinguished_name = req_distinguished_name
attributes = req_attributes
prompt = no
[ req_distinguished_name ]
C = $SSL_LOCATION_COUNTRY
ST = $SSL_LOCATION_STATE
L = $SSL_LOCATION_LOCALITY
O = $SSL_ORG_NAME
OU = $SSL_OU_NAME
CN = $SSL_SERVER_NAME
emailAddress = $SSL_CONTACT_EMAIL
[ req_attributes ]
challengePassword = A challenge password
EOF

SSLEAYCHROOT=`echo $SSLEAY | sed "s=/target=="`
$ROOTCMD bash -c "openssl req -config $SSLEAYCHROOT -x509 -newkey rsa:1024 -keyout $KEY -out $CERT -days $DAYS -nodes"

rm -f $SSLEAY
rm -f $RANDFILE

$ROOTCMD bash -c "chown root:Debian-exim $KEY $CERT"
$ROOTCMD bash -c "chmod 640 $KEY $CERT"

