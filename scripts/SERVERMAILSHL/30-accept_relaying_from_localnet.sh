#! /bin/sh

# let people on the local network relay mail through this server

## Our "die" function (think perl)
function die () { echo "$@" 1>&2 ; exit 1 ; }

# check to make sure our variables have been declared
[ ! -n "$FW_INTERNAL_NETWORK" ] && { die "ERROR: FW_INTERNAL_NETWORK NOT FOUND!" ; }
[ ! -n "$FW_INTERNAL_NETWORK_SUBNET_SHORT" ] && { die "ERROR: FW_INTERNAL_NETWORK_SUBNET_SHORT NOT FOUND!" ; }

# set the system mailname, domainname, and debconf entries to indicate THIS is the primary domain of the system.

$ROOTCMD bash -c "echo 'exim4-config    exim4/dc_relay_nets        string  $FW_INTERNAL_NETWORK/$FW_INTERNAL_NETWORK_SUBNET_SHORT' | debconf-set-selections"
