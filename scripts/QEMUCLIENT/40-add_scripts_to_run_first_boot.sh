#!/bin/sh

# copy in a script that performs security updates.
fcopy /etc/init.d/perform_security_updates

# register it with the init system.
$ROOTCMD insserv -d perform_security_updates

# copy in a sources.list containing only security sources, to prevent distro upgrades.
fcopy /etc/apt/sources.list.security