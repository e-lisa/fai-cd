#! /bin/bash
$ROOTCMD a2dismod cgi
$ROOTCMD a2dismod authz_default
$ROOTCMD a2dismod authz_groupfile
$ROOTCMD a2dismod autoindex
# MRTG uses mod_dir
#$ROOTCMD a2dismod dir

# required for streaming
ifclass SERVERZONEMINDER && $ROOTCMD a2enmod cgi
