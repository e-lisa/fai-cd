#!/bin/bash
# 40-make_exim_reject_trash.sh
#
# drop-in replacement for 40-make_exim_reject_trash script.

## Our "die" function
function die () { echo "$@" 1>&2 ; exit 1 ; }

[ ! -n "$target" ] && die "variable \$target not defined!"

if [ -f $target/etc/exim4/conf.d/acl/40_exim4-config_check_data ]; then
    {
        if [ -n "`cat $target/etc/exim4/conf.d/acl/40_exim4-config_check_data| sed -n \"/# accept otherwise/{n;p;}\"`" ]; then
            if [ "`cat $target/etc/exim4/conf.d/acl/40_exim4-config_check_data| sed -n \"/# accept otherwise/{n;p;}\"`" != "# cfengine - scripts/SERVERMAILPARANOID/40-make_exim_reject_trash" ]; then
                {
                    cat $target/etc/exim4/conf.d/acl/40_exim4-config_check_data| sed "s/# accept otherwise/# cfengine - scripts\/SERVERMAILPARANOID\/40-make_exim_reject_trash\n  # Reject messages that have serious MIME errors.\n  # this calls the demime condition again, but it will return cached results.\n  deny message = Serious Mime Defect Detected(\$demime_reason)\n  demime = *\nCFENGINE_BROKENNESS\n  .ifdef TEERGRUBE\n    delay = TEERGRUBE\n  .endif\n \n  # Reject file extensions used by worms\n  # Note that the extension list may be incomplete\n  deny message = This domain has a policy of not accepting certain types of attatchments in mail as they may contain a virus. this \\\\\\\\\n                 mail has a file with a .\$found_extension attatchment and it is not accepted. If you have a legitimate need to send\\\\\\\\\n                 this paticular attachment, send it in a compressed archive, and it will then be forwarded to the recipient.\n  # Note: jack uses .exe, .vbs, and .bat on a day-to-day basis.\n  demime= com:pif:scr\n  .ifdef TEERGRUBE\n    delay = TEERGRUBE\n  .endif\n \n  # Reject messages containing malware.\n  deny message = This message contains a virus (\$malware_name) and has been rejected\n  malware = *\n  .ifdef TEERGRUBE\n    delay = TEERGRUBE\n  .endif\n# accept otherwise\n/" > /tmp/jtemp
		    cat /tmp/jtemp > $target/etc/exim4/conf.d/acl/40_exim4-config_check_data
                  }
            fi
	else die "could not find our spot in $target/etc/exim4/conf.d/acl/40_exim4-config_check_data not found!"
        fi
    }
    else die "$target/etc/exim4/conf.d/acl/40_exim4-config_check_data not found!"
fi
