#! /bin/bash
$ROOTCMD a2dismod cgi
$ROOTCMD a2dismod authz_groupfile
$ROOTCMD a2dismod reqtimeout

# FIXME: really bastionize wordpress in jessie.
# $ROOTCMD a2dismod autoindex
# $ROOTCMD a2dismod setenvif
# $ROOTCMD a2dismod authz_default

# wordpress uses moddir
# $ROOTCMD a2dismod dir

